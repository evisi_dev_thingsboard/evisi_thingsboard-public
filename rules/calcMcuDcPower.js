function transform(msg, metadata, msgType) {
	// Get mot_1_mcuDcCurrent from msg
	var mot_1_mcuDcCurrent = msg.mot_1_mcuDcCurrent;

	// Get mot_1_mcuDcVoltage from metadata
	var mot_1_mcuDcVoltage = metadata.mot_1_mcuDcVoltage;

	// Calculate powerValue
	var powerValue = parseFloat(mot_1_mcuDcCurrent * mot_1_mcuDcVoltage);

	// Add powerValue to the message
	msg.mot_1_mcuDcPower = powerValue;

	return {msg: msg, metadata: metadata, msgType: msgType} ;
}
