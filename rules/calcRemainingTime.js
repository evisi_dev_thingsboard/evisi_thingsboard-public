function transform(msg, metadata, msgType) {
	// Get remaining distance from msg
	veh_1_distanceRemaing_i = msg.veh_1_distanceRemaing_i;

	// Get speed sample values from metadata
	var gps_1_speed_data = JSON.parse(metadata.gps_1_speed);

	// Calculate average speed for gps_1_speed sample values
	var gps_1_speed_avg = Array.isArray(gps_1_speed_data)
		  ? gps_1_speed_data.reduce(function(a, b) {
		          return a + b.value
		      }, 0) / gps_1_speed_data.length
		  : parseFloat(gps_1_speed_data);

	// Calculate remaining time and convert to seconds
	var veh_1_timeRemaing_i = (veh_1_distanceRemaing_i / gps_1_speed_avg) * 3600;

	// Append the new data to the message
	msg.veh_1_timeRemaing_i = Math.round(veh_1_timeRemaing_i);

	// clean metadata
	metadata.gps_1_speed = null;

	return {msg: msg, metadata: metadata, msgType: msgType};
}
