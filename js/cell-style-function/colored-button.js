
function cellStyle(value) {
	var statusClasification = [
		  /*
		  * Use values to define ranges and colors
		  * If a value can go down to negative infinite, do not set the from property
		  * for the color desired
		  * If a value can go up to infinite, do not set the to property for the color
		  * desired
		  *
		  * For colors you can use any valid CSS color in any format
		  */
		  {
		      to: 200,
		      color: "green"
		  },
		  {
		      from: 201,
		      to: 400,
		      color: "yellow"
		  },
		  {
		      from: 401,
		      color: "red"
		  }
	];
		  
	color = "white";
	for (var status of statusClasification) {
		  if (typeof status.from == 'undefined' && value <= status.to
		      || status.from <= value && value <= status.to
		      || status.from <= value && typeof status.to == 'undefined') {
		          console.log('entra');
		          color = status.color;
		          break;
		      }
	}
	return {
		"background": "linear-gradient(" + color + ", " + color + ")",
		"color": "#333",
		"padding": "0 20px",
		"text-align": "center",
		  "background-clip": "content-box"
	};
}
